#!/bin/sh

# Run the command, collecting the output into a variable
RET="$(mount -o ${1},remount /)"

# Returns any errors encountered, e.g. mount is busy
echo "${RET}"
