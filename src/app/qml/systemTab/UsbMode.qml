/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Layouts 1.1
import TweakTool 1.0

import "../components"
import "../components/ListItems" as ListItems
import "../js/shell.js" as Shell

Page {
    id: rootItem

    header: PageHeader {
        title: i18n.tr("ADB settings")
        flickable: view.flickableItem
    }

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            width: view.width

            ListItems.SectionDivider {
                iconName: "stock_usb"
                text: i18n.tr("USB mode")
            }

            ListItems.OptionSelector {
                id: selector
                model: [
                    i18n.tr("MTP - Media Transfer Protocol"),
                    i18n.tr("RNDIS - Remote Network Driver Interface Specification")
                ]

                function getCurrentIndex() {
                    var checkMTP = Shell.cmdAndroidGadgetService("status", "mtp");
                    var checkRNDIS = Shell.cmdAndroidGadgetService("status", "rndis");
                    var checkTethering = Shell.cmdTetheringCheckEnabled();

                    // Default to unselected if neither service detected
                    var currentIndex = -1;

                    if (Shell.processLaunch(checkMTP.cmd).indexOf("mtp enabled") > -1) {
                        currentIndex = 0;
                    } else if (Shell.processLaunch(checkRNDIS.cmd).indexOf("rndis enabled") > -1) {
                        // Furthermore, check if `tethering` is enabled
                        if (Shell.processLaunch(checkTethering.cmd).indexOf("regular file") > -1) {
                            currentIndex = 1;
                        }
                    }

                    return currentIndex
                }

                Component.onCompleted: {
                    selectedIndex = getCurrentIndex();
                }

                onSelectedIndexChanged: {
                    var currentIndex = getCurrentIndex();
                    var enableMTP = Shell.cmdAndroidGadgetService("enable", "mtp");
                    var enableRNDIS = Shell.cmdAndroidGadgetService("enable", "rndis");
                    var tethering_action = '';

                    // Only run the relevant commands if there is a change to be made
                    if (selectedIndex !== currentIndex) {
                        if (selectedIndex == 0) {
                            tethering_action = "disable";
                            Shell.processLaunch(enableMTP.cmd);
                        } else if (selectedIndex == 1) {
                            tethering_action = "enable";
                            Shell.processLaunch(enableRNDIS.cmd);
                        }

                        // Enable/disable `tethering` as required
                        var toggleTetheringRNDIS = Shell.cmdTetheringRNDIS(
                            tethering_action
                        );
                        Shell.processLaunch(
                            toggleTetheringRNDIS.cmd,
                            toggleTetheringRNDIS.sudo
                        );
                    }
                }
            }

            ListItems.SectionDivider {
                iconName: "ubuntu-sdk-symbolic"
                text: i18n.tr("Debugging")
            }

            ListItem {
                ListItemLayout {
                    anchors.fill: parent
                    title.text: i18n.tr("Revoke USB debugging authorizations")
                }
                onClicked: PopupUtils.open(revokeAdbDialog)
            }
        }
    }

    SystemFile {
        id: adb_keys
        filename: "/data/misc/adb/adb_keys"
        onPasswordRequested: providePassword(pam.password)
    }

    Component {
        id: revokeAdbDialog

        Dialog {
            id: revokeAdbDialogue

            title: i18n.tr("Revoke USB debugging authorizations")
            text: i18n.tr("Revoke access to USB debugging from all computers you've previously authorized?")

            RowLayout {
                width: parent.width
                spacing: units.gu(1)

                Button {
                    Layout.fillWidth: true
                    text: i18n.tr("Cancel")
                    onClicked: PopupUtils.close(revokeAdbDialogue);
                }

                Button {
                    Layout.fillWidth: true
                    text: i18n.tr("OK")
                    color: theme.palette.normal.positive
                    onClicked: {
                        if (adb_keys.exists)
                            adb_keys.rm()

                        PopupUtils.close(revokeAdbDialogue);
                    }
                }
            }
        }
    }
}
